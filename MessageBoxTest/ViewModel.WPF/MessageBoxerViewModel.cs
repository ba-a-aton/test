﻿using MessageBoxer.View;

namespace MessageBoxer.ViewModel.WPF
{
    public class MessageBoxerViewModel: BaseViewModel, IMessageBoxerViewModel
    {
        public MessageBoxerViewModel(IMessageBoxService service)
        {
            this.service = service;
        }

        public IMessageBoxService service { get; set; }

        public Command ClickCommand
        {
            get { return new Command(() => { service.ShowMessage("Hello world"); }); }
        }
    }
}
