﻿using MessageBoxer.View;
using MessageBoxer.View.WPF;
using MessageBoxer.ViewModel;
using MessageBoxer.ViewModel.WPF;
using Microsoft.Practices.Unity;
using System;

namespace MessageBoxer
{
    class Startup
    {
        [STAThread]
        public static void Main()
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<IMessageBoxerView, MessageBoxerView>();
            container.RegisterType<IMessageBoxerViewModel, MessageBoxerViewModel>();
            container.RegisterType<IMessageBoxerApplication, MessageBoxerApplication>();
            container.RegisterType<IMessageBoxService, MessageBox>();
            
            var view = container.Resolve<IMessageBoxerView>();
            var application = container.Resolve<IMessageBoxerApplication>();
            application.Run();
        }
    }
}
