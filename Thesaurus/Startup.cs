﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thesaurus.Model;
using Thesaurus.View;
using Thesaurus.View.WPF;
using Thesaurus.ViewModel;
using Thesaurus.ViewModel.WPF;

namespace Thesaurus
{
    class Startup
    {
        static IThesaurusApplication app;

        [STAThread]
        public static void Main()
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<IThesaurusApplication, ThesaurusApplication>();
            container.RegisterType<IThesaurusView, ThesaurusView>();
            container.RegisterType<IThesaurusViewModel, ThesaurusViewModel>();
            container.RegisterType<IThesaurusModel, ThesaurusModel>();

            app = container.Resolve<IThesaurusApplication>();
            app.Run();
        }
    }
}
