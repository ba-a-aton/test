﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Thesaurus.ViewModel
{
    public interface IThesaurusViewModel
    {
        IEnumerable<string> Words { get; set; }
        IEnumerable<string> Synonyms { get; set; }
        string SelectedWord { get; set; }
        ThesaurusViewMode ViewMode {get; set;}
        IThesaurus Thesaurus { get; set; }
        IEnumerable<string> WordsToAdd { get; set; }
        string CurrentWord { get; set; }
        ICommand ChangeModeCommand { get; }
        ICommand AddSynonymsCommand { get; }
        ICommand CancelSynonymsCommand { get; }
        ICommand AddSynonymToListCommand { get; }
    }
}
