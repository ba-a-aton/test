﻿using MessageBoxer.ViewModel;

namespace MessageBoxer.View
{
    public interface IMessageBoxerView
    {
        //void ShowWindow(IMessageBoxerViewModel viewModel);
        void Show();
    }
}
