﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesaurus.ViewModel
{
    public interface IWord
    {
        IEnumerable<IWord> Synonyms { get; }
        string Word { get; }
    }
}
