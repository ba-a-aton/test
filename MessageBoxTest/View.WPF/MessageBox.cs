﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MessageBoxer.View.WPF
{
    public class MessageBox : IMessageBoxService
    {
        public void ShowMessage(string text)
        {
            System.Windows.MessageBox.Show(text);
        }
    }
}
