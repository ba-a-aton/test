﻿using MessageBoxer.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageBoxer.ViewModel
{
    public interface IMessageBoxerViewModel
    {
        IMessageBoxService service { get; set; }
    }
}
