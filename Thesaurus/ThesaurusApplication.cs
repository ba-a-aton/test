﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Thesaurus.View;

namespace Thesaurus
{
    class ThesaurusApplication:Application, IThesaurusApplication
    {
        public ThesaurusApplication(IThesaurusView view)
        {
            view.Show();
        }
    }
}
