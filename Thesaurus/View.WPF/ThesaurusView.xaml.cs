﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Thesaurus.ViewModel;

namespace Thesaurus.View.WPF
{
    /// <summary>
    /// Interaction logic for ThesaurusView.xaml
    /// </summary>
    public partial class ThesaurusView : Window, IThesaurusView
    {
        public ThesaurusView(IThesaurusViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
    }
}
