﻿using MessageBoxer.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageBoxer
{
    interface IMessageBoxerApplication
    {         
        int Run();
        //void Init(IMessageBoxerView view);
    }
}
