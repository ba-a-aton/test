﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Thesaurus.ViewModel.WPF
{
    using ThesaurusDictionary = Dictionary<string, IEnumerable<string>>;
    public class Thesaurus : ThesaurusBaseViewModel, IThesaurus
    {
        public ThesaurusDictionary Words = new Dictionary<string, IEnumerable<string>>();

        public void AddSynonyms(IEnumerable<string> synonyms)
        {
            if (synonyms != null)
            {
                ///task for values where in dictionary before
                Task oldValuesTask = new Task(() =>
                addToOld(synonyms.Where(x => Words.Keys.Contains(x))
                .GroupBy(x => x).Select(y => y.First()),
                synonyms.GroupBy(x => x).Select(y => y.First())));

                ///task for words weren't in dictionary before
                Task <ThesaurusDictionary> newValuesTask = new Task<ThesaurusDictionary>(() =>
                 addNew(synonyms.Where(x => !Words.Keys.Contains(x))
                 .GroupBy(x => x).Select(y => y.First()), 
                 synonyms.GroupBy(x => x).Select(y => y.First())));

                oldValuesTask.Start();
                newValuesTask.Start();

                ///waiting tasks finish
                Task.WaitAll(new[] { oldValuesTask, newValuesTask });

                ///concatination of results
                Words = Words.Concat(newValuesTask.Result).ToDictionary(pair => pair.Key, pair => pair.Value);
            }
        }

        /// <summary>
        /// Method to add words wheren't in list before
        /// </summary>
        /// <param name="newWords">list of new words</param>
        /// <param name="allWords">list of all words to add as synonyms</param>
        /// <returns>new dictionary of words and synonyms</returns>
        private ThesaurusDictionary addNew(IEnumerable<string> newWords, IEnumerable<string> allWords)
        {
            ThesaurusDictionary newDictionary = new ThesaurusDictionary();
            foreach(string source in newWords)
            {
                ///adding synonyms to word without itself
                newDictionary.Add(source, allWords.Where(x => x != source));
            }
            return newDictionary;
        }


        private void addToOld(IEnumerable<string> oldWords, IEnumerable<string> allWords)
        {
            foreach (string source in oldWords)
            {
                var sequence = Words.Where(x => x.Key == source).First();
                var s1 = sequence.Value.Concat(allWords.Where(x => x != source && !sequence.Value.Contains(x)));
                Words[source] = s1;
            }
        }


        public IEnumerable<string> GetSynonyms(string word)
        {
            return (word!=null && Words.Keys.Contains(word))? (from pair in Words where word == pair.Key select pair.Value).First()
                :new List<string>();
        }


        public IEnumerable<string> GetWords()
        {
            return Words.Keys;
        }
    }
}
