﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MessageBoxer.ViewModel;

namespace MessageBoxer.View.WPF
{
    /// <summary>
    /// Interaction logic for MessageBoxerTextView.xaml
    /// </summary>
    public partial class MessageBoxerView : Window, IMessageBoxerView
    {
        public MessageBoxerView(IMessageBoxerViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
    }
}
