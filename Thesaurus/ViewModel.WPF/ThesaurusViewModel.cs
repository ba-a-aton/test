﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thesaurus.Model;

namespace Thesaurus.ViewModel.WPF
{
    public class ThesaurusViewModel : ThesaurusBaseViewModel, IThesaurusViewModel
    {

        IEnumerable<string> words;
        string selectedWord;
        IEnumerable<string> synonyms;
        ObservableCollection<string> synonymsToAdd = new ObservableCollection<string>();
        ThesaurusViewMode viewMode = ThesaurusViewMode.List;
        string currentSynonym = string.Empty;

        public ThesaurusViewModel(IThesaurusModel model)
        {
            IEnumerable<string> admirationList =
             new[] { "admiration", "affection", "adoration", "delight", "appreciation" };
            IEnumerable<string> adorationList =
                new[] { "adoration", "admiration", "delight", "veneration" };
            IEnumerable<string> trumpList =
                new[] { "trump", "asset", "convenience", "avail", "avail" };

            Thesaurus.AddSynonyms(admirationList);
            Thesaurus.AddSynonyms(adorationList);
            Thesaurus.AddSynonyms(trumpList);
            words = Thesaurus.GetWords();
            selectedWord = words.Count() != 0 ? words.First() : string.Empty;

        }

        public IThesaurus Thesaurus { get; set; } = new Thesaurus();

        public IEnumerable<string> Words
        {
            get { return Thesaurus.GetWords(); }
            set
            {
                words = value;
                NotifyPropertyChanged("Words");
            }
        }

        public IEnumerable<string> Synonyms
        {
            get { return Thesaurus.GetSynonyms(selectedWord); }
            set
            {
                synonyms = value;
                NotifyPropertyChanged("Synonyms");
            }
        }

        public ObservableCollection<string> SynonymsToAdd
        {
            get { return synonymsToAdd; }
            set
            {
                synonymsToAdd = value;
                NotifyPropertyChanged("SynonymsToAdd");
            }
        }

        public string CurrentSynonym
        {
            get { return currentSynonym; }
            set
            {
                currentSynonym = value;
                NotifyPropertyChanged("CurrentSynonym");
            }
        }

        /// <summary>
        /// property for selected word in list
        /// also we need to update Synonyms property
        /// to notify changes for UI
        /// </summary>
        public string SelectedWord
        {
            get { return selectedWord; }
            set
            {
                selectedWord = value;
                NotifyPropertyChanged("SelectedWord");
                NotifyPropertyChanged("Synonyms");
            }
        }

        public ThesaurusViewMode ViewMode
        {
            get { return viewMode; }
            set
            {
                viewMode = value;
                NotifyPropertyChanged("ViewMode");
            }
        }
        
        public Command ChangeModeCommand
        {
            get { return new Command(() => { ViewMode = ThesaurusViewMode.Add; }); }
        }

        public Command AddSynonymsCommand
        {
            get { return new Command(() => {
                Thesaurus.AddSynonyms(SynonymsToAdd);
                ViewMode = ThesaurusViewMode.List;
                CurrentSynonym = string.Empty;
                SynonymsToAdd = new ObservableCollection<string>();
                NotifyPropertyChanged("Words");
            });  }
        }

        public Command CancelSynonymsCommand
        {
            get
            {
                return new Command(() => {
                    ViewMode = ThesaurusViewMode.List;
                    CurrentSynonym = string.Empty;
                    SynonymsToAdd = new ObservableCollection<string>();
                    NotifyPropertyChanged("Words");
                });
            }
        }

        public Command AddSynonymToListCommand
        {
            get
            {
                return new Command(() =>
                {
                    if (currentSynonym != string.Empty)
                    {
                        SynonymsToAdd.Add(currentSynonym);
                        CurrentSynonym = string.Empty;
                    }
                });
            }
        }

        public void ReadData()
        {

        }

        
    }
}
