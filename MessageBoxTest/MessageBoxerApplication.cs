﻿using MessageBoxer.View;
using MessageBoxer.View.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MessageBoxer
{
    public class MessageBoxerApplication: Application, IMessageBoxerApplication
    {
        public MessageBoxerApplication(IMessageBoxerView view)
        {
            view.Show();
        }
    }
}
