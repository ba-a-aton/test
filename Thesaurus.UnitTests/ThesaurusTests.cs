﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Thesaurus.ViewModel.WPF;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Thesaurus.UnitTests
{
    [TestClass]
    public class ThesaurusTests
    {
        Thesaurus.ViewModel.WPF.Thesaurus thesaurus = new Thesaurus.ViewModel.WPF.Thesaurus();
        IEnumerable<string> admirationList = 
            new[] { "admiration", "affection", "adoration", "delight", "appreciation" };
        IEnumerable<string> adorationList =
            new[] { "adoration", "admiration", "delight", "veneration" };
        IEnumerable<string> trumpList =
            new[] { "trump", "asset", "convenience", "avail", "avail"};


        [TestMethod]
        public void AddSynonymsTests()
        {
            thesaurus.AddSynonyms(null);
            thesaurus.AddSynonyms(admirationList);
            thesaurus.AddSynonyms(adorationList);//has some common words to first list
            thesaurus.AddSynonyms(trumpList);//has no common words. also has 2 duplicates            
        }

        [TestMethod]
        public void GetSynonymsTests()
        {
            thesaurus.AddSynonyms(admirationList);
            thesaurus.AddSynonyms(adorationList);
            thesaurus.AddSynonyms(trumpList);
            thesaurus.GetSynonyms(null);
            thesaurus.GetSynonyms("deer");//word isn't in dictionary

            foreach (string word in admirationList)
            {
                List<string> result = thesaurus.GetSynonyms(word).ToList();
                foreach(string subword in admirationList.Where(x=>x!=word))
                {
                    Assert.IsTrue(result.Contains(subword));
                }
            }
        }

        [TestMethod]
        public void GetWordsTests()
        {
            thesaurus.GetWords();//for empty dictionary
            Assert.IsNotNull(thesaurus.GetWords());
            Assert.IsInstanceOfType(thesaurus.GetWords(), typeof(IEnumerable<string>));
            thesaurus.AddSynonyms(admirationList);
            thesaurus.AddSynonyms(adorationList);
            thesaurus.AddSynonyms(trumpList);

            List<string> result = thesaurus.GetWords().ToList();

            ///is including all lists
            foreach (string word in admirationList) { Assert.IsTrue(result.Contains(word)); }
            foreach (string word in adorationList) { Assert.IsTrue(result.Contains(word)); }
            foreach (string word in trumpList) { Assert.IsTrue(result.Contains(word)); }
        }
    }
}
