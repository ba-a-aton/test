﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesaurus.View
{
    public interface IThesaurusView
    {
        void Show();
    }
}
